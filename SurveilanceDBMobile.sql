if not exists (select * from sys.databases where name = N'SurveilanceDBMobile')
begin
	create database SurveilanceDBMobile
end
go

use SurveilanceDBMobile
go

create table ModelLicencePlates(
ID int identity primary key,
plate nvarchar(25) not null,
time nvarchar(50) default getdate(),
location nvarchar(50) not null,
notified bit default 0
)
go

create procedure insertPlate
@plate nvarchar(25),
@location nvarchar(50),
@time nvarchar(50)
as
begin
	insert into ModelLicencePlates
	values (@plate,@time,@location,default)
end
go

create procedure updateNotified
@plate nvarchar(25)
as
begin
	update ModelLicencePlates
	set notified=1 where plate=@plate
end
go

create procedure getHitPrivate
@plate nvarchar(25),
@exists bit output,
@location nvarchar(100) output,
@time nvarchar(50) output
as
begin
	if exists (select top 1 plate from ModelLicencePlates where plate=@plate and notified=0 order by time desc)
		begin
		select @exists=1
		select @location=(select top 1 location from ModelLicencePlates where plate=@plate and notified=0 order by time desc)
		select @time=(select top 1 time from ModelLicencePlates where plate=@plate and notified=0 order by time desc)
		exec updateNotified @plate
		end
	else
		begin
		select @exists=0
		end
end
go

create procedure getHit
@plate nvarchar(25)
as
begin
declare @exists bit
declare @location nvarchar(100)
declare @time nvarchar(50)
exec getHitPrivate @plate, @exists output,@location output,@time output
select @exists as hit,@location as loc,@time as dat
end
go

create procedure restoreNotified
as
begin
update ModelLicencePlates
set notified=0
end
go

exec insertPlate 'zg1234ab','locationA','2020-01-09 123456'
exec insertPlate 'vz555fc','locationB'
exec insertPlate 'ri765jk','locationC'
exec insertPlate 'zg1234ab','locationB'

exec getHit 'zg1234ab'