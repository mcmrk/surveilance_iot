package com.example.surveillance;

import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.LinearInterpolator;
import android.view.animation.RotateAnimation;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import retrofit2.Retrofit;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.UnknownHostException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import static java.lang.Thread.sleep;

public class MainActivity extends AppCompatActivity {

    public static MainActivity mainActivity;
    public static Boolean isVisible = false;
    private static final String TAG = "MainActivity";
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static Retrofit retrofit = null;
    final String fileName = "plates";
    final ArrayList<String> lvItems = new ArrayList<String>();
    ArrayList<String> plates = new ArrayList<String>();
    private String plate = "";
    private String res = "";
    private Hit hit;
    public TextView textView;
    public TextView txtMessage;
    private TransparentProgressDialog pd;
    private Handler h;
    private Runnable r;
    public List<String> messages = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        loadPlates();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mainActivity = this;
        h = new Handler();
        pd = new TransparentProgressDialog(this, R.drawable.spinner);
        r = new Runnable() {
            @Override
            public void run() {
                if (pd.isShowing()) {
                    pd.dismiss();
                }
            }
        };
        final Button addBtn = (Button) findViewById(R.id.addBtn);
        addBtn.setText("PROVJERA");
        final ListView lvPlates = (ListView) findViewById(R.id.lvPlates);
        final ArrayAdapter lvAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, lvItems);
        lvPlates.setAdapter(lvAdapter);
        final EditText LicencePlateeditText = (EditText) findViewById(R.id.LicencePlateeditText);
        LicencePlateeditText.setText("");
        textView = (TextView) findViewById(R.id.textView);
        txtMessage = (TextView) findViewById(R.id.txtMessage);
        final Button btnDelete = (Button) findViewById(R.id.btnDelete);
        btnDelete.setText("BRISANJE");
        textView.setText("");
        txtMessage.setText("");
        createNotificationChannel();
        addBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LicencePlateeditText.getText().length() > 0 || LicencePlateeditText.getText() == null) {
                    textView.setText("");
                    String plateText = LicencePlateeditText.getText().toString();
                    if (!plateText.equals("")) {
                        if (plateText.matches("^[a-zA-Z0-9-]*$")) {
                            if (checkPlate(plateText)) {
                                lvAdapter.add(plateText.toUpperCase());
                                LicencePlateeditText.setText("");
                                textView.setText("Uspješno dodavanje registarske oznake");
                            }
                        } else {
                            textView.setText("Molimo unesite registarsku oznaku u ispravnom formatu");
                        }
                    } else {
                        textView.setText("Molimo unesite valjanu registarsku oznaku");
                    }
                    plates = lvItems;
                } else {
                    messages.clear();
                    plates = lvItems;
                    new newThread().execute();
                    pd.show();
                    h.postDelayed(r, 5000);
                    textView.setText("Provjera u tijeku...");
                }
            }
        });

        lvPlates.setAdapter(lvAdapter);
        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lvItems.clear();
                lvAdapter.notifyDataSetChanged();
            }
        });

        LicencePlateeditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (LicencePlateeditText.getText().length() > 0 || LicencePlateeditText.getText() == null) {
                    addBtn.setText("DODAJ");
                } else {
                    addBtn.setText("PROVJERA");
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
    }

    @Override
    protected void onPause() {
        savePlates(lvItems);
        super.onPause();

        isVisible = false;
    }

    private boolean checkPlate(String plateText) {

        return true;
    }


    public void savePlates(ArrayList<String> lvItems) {
        try {
            String dir = getApplicationContext().getFilesDir().getPath().toString() + "/" + fileName + ".txt";
            final File myFile = new File(dir);
            if (!myFile.exists())
                myFile.createNewFile();
            FileWriter writer = new FileWriter(myFile);
            for (String str : lvItems) {
                writer.write(str);
                writer.write(String.format("%n"));
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadPlates() {
        try {
            String dir = getApplicationContext().getFilesDir().getPath().toString() + "/" + fileName + ".txt";
            Log.e("TAG", dir);
            final File myFile = new File(dir);
            if (!myFile.exists())
                Log.e("TAG", "could not create the directories");
            BufferedReader br = new BufferedReader(new FileReader(myFile));
            String line;
            while ((line = br.readLine()) != null) {
                lvItems.add(line);
            }
            br.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        isVisible = true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isVisible = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        isVisible = false;
    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.notChannel);
            String description = getString(R.string.notDesc);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("1", name, importance);
            channel.setDescription(description);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    private void showNotification(String message, int notId) {
        NotificationCompat.Builder builder = new NotificationCompat.Builder(mainActivity);
        builder.setContentTitle("Hit")
                .setContentText(message)
                .setSmallIcon(R.drawable.common_google_signin_btn_icon_light_normal)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setChannelId("1")
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC);
        NotificationManagerCompat notificationManager = NotificationManagerCompat.from(mainActivity);
        notificationManager.notify(notId, builder.build());
    }

    public void apiCall() throws UnknownHostException {
        String urlPath="https://webhooks.mongodb-stitch.com/api/client/v2.0/app/surveillanceiot-znruf/service/SurveillanceAPI/incoming_webhook/read";
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        Response.Listener<JSONArray> listener=new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                try {
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                JSONArray jsonArray=response;
                for (String p:plates) {
                    plate=p;
                    for (int i = 0; i < jsonArray.length(); i++) {
                        try {
                            JSONObject row = jsonArray.getJSONObject(i);
                            if (row.getString("plate").equals(plate)) {
                                res = row.toString();
                                res = res.replace("[", "");
                                res = res.replace("]", "");
                                generateHitObject(res, plate);
                                break;
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        };
        Response.ErrorListener errorListener=new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("response",error.toString());
            }
        };
        JsonArrayRequest objectRequest=new JsonArrayRequest(urlPath,listener,errorListener);
        requestQueue.add(objectRequest);
    }

    private void generateHitObject(String response,String plate){
        if(response!=""&&response!=null&&!response.isEmpty()) {
            try {
                JSONObject json = new JSONObject(response);
                hit.setLocation(json.getString("location"));
                hit.setTime(json.getString("time"));
                hit.setPlate(json.getString("plate"));
                //String message = "Registarska oznaka " + hit.getPlate() + " prepoznata je na lokaciji " + hit.getLocation() + " na datum " + hit.getTime();
                SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HHmmss");
                String t="";
                try {
                    Date d=sdf.parse(hit.getTime());
                    sdf.applyPattern("dd.MM.yyyy. HH:mm");
                    t=sdf.format(d);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                String message=hit.getPlate()+" LOCATION: "+hit.getLocation()+" TIME: "+ t;
                messages.add(message);
                Log.e("hit", hit.getPlate());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public class newThread extends AsyncTask<Void,Void,Void>{
        MainActivity main=mainActivity;
        @RequiresApi(api = Build.VERSION_CODES.O)
        @Override
        public Void doInBackground(Void... voids) {
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                hit=new Hit();
                try {
                    main.apiCall();
                } catch (UnknownHostException e) {
                    e.printStackTrace();
                }
                try {
                    TimeUnit.SECONDS.sleep(1);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            int notCount=1;
            for (String s:messages) {
                showNotification(s,notCount);
                notCount++;
            }
            textView.setText("");
        }
    }

    private class TransparentProgressDialog extends Dialog {

        private ImageView iv;

        public TransparentProgressDialog(Context context, int resourceIdOfImage) {
            super(context, R.style.TransparentProgressDialog);
            WindowManager.LayoutParams wlmp = getWindow().getAttributes();
            wlmp.gravity = Gravity.CENTER_HORIZONTAL;
            getWindow().setAttributes(wlmp);
            setTitle(null);
            setCancelable(false);
            setOnCancelListener(null);
            LinearLayout layout = new LinearLayout(context);
            layout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
            iv = new ImageView(context);
            iv.setImageResource(resourceIdOfImage);
            layout.addView(iv, params);
            addContentView(layout, params);
        }

        @Override
        public void show() {
            super.show();
            RotateAnimation anim = new RotateAnimation(0.0f, 360.0f , Animation.RELATIVE_TO_SELF, .5f, Animation.RELATIVE_TO_SELF, .5f);
            anim.setInterpolator(new LinearInterpolator());
            anim.setRepeatCount(Animation.INFINITE);
            anim.setDuration(3000);
            iv.setAnimation(anim);
            iv.startAnimation(anim);
        }
    }
}
