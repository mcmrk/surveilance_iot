<b>Install OpenALPR by following these steps:</b>

<b>1.</b>	Unzip the binaries and the source code in the same directory (for example C:/OpenALPR). First unzip binaries (openalpr-2.3.0-win-64bit) then unzip source files (openalpr-2.3.0) in that same folder, overwriting all conflicts. 

<b>2.</b>	Add that directory to your PATH (Enviorment variables -> User Variables -> Path)

<b>3.</b>	Open Anaconda Prompt, than set for C:\OpenALPR\src\bindings\pythonsetup.py install

<b>4.</b>	OpenALPR should now be installed and in pip list (pip show openalpr, v.1.0)

<b>5.</b>	Copy openalpr.conf file in C:/OpenALPR (folder from step 1.) and overwrite file

<b>6.</b>	Open Jupyter and run notebook file (everything should work now)
