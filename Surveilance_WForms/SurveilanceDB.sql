if not exists (select * from sys.databases where name = N'SurveilanceDB')
begin
	create database SurveilanceDB
end
go

use SurveilanceDB
go

create table Camera(
ID int identity primary key,
Location nvarchar(100),
Enabled bit,
PTZOption bit,
ALPROption bit,
channel int
)
go

create table PTZValues(
ID int identity primary key,
CameraID int foreign key references Camera(ID),
Pan int not null,
Tilt int not null,
Zoom int not null
)
go

create procedure ToggleCamera
@CameraID int
as
begin
	update Camera
	set
		Enabled = case when Enabled='True' then 'False' else 'True' end
	where ID=@CameraID
end
go

insert into Camera values 
('Algebra','False','False','False'),
('Algebra','False','True','False'),
('Algebra','False','False','False'),
('Velesajam','False','False','True')
go

drop table PTZValues
drop table Camera
go

select * from Camera
