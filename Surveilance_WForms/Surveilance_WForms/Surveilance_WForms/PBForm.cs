﻿using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Surveilance_WForms
{
    public partial class PBForm : Form
    {
        private Camera camera;
        private Surveilance_App app;
        private Int32 m_lPlayHandle = -1;
        private Int32 m_lUserID = -1;
        private uint iLastErr = 0;
        private string str;
        public CHCNetSDK.NET_DVR_DEVICEINFO_V30 DeviceInfo;
        public CHCNetSDK.NET_DVR_IPPARACFG_V40 m_struIpParaCfgV40;
        public CHCNetSDK.NET_DVR_GET_STREAM_UNION m_unionGetStream;
        public CHCNetSDK.NET_DVR_IPCHANINFO m_struChanInfo;
        private bool videoPlaying = false;
        //public const String rootPath = "H:\\Fax\\Algebra\\4. godina\\IoT\\surveilance_iot\\Surveilance_WForms\\Surveilance_WForms\\Surveilance_WForms\\Screenshots\\";
        public const String rootPath = "E:\\Fax\\Algebra\\4. godina\\IoT\\Surveilance_project\\Surveilance_WForms\\Surveilance_WForms\\Surveilance_WForms\\Screenshots\\";
        public PBForm(Camera camera,Surveilance_App app)
        {
            InitializeComponent();
            this.camera = camera;
            this.app = app;
            dtpStart.Value = DateTime.Now;
            dtpStart.MinDate = DateTime.Now.AddDays(-7);
            dtpStart.MaxDate = DateTime.Now;
            dtpEnd.Enabled = false;
            btnPBFast.Enabled = false;
            btnPBPlay.Enabled = false;
            btnPBSlow.Enabled = false;
            btnPBStop.Enabled = false;
        }

        private void PBForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            this.app.Show();
        }

        private void dtpStart_ValueChanged(object sender, EventArgs e)
        {
            dtpEnd.Enabled = true;
            dtpEnd.MinDate = dtpStart.Value;
            dtpEnd.MaxDate = DateTime.Now;
        }

        private void btnPBPlay_Click(object sender, EventArgs e)
        {
            if (!videoPlaying)
            {
                btnPBCapture.Enabled = true;
                videoPlaying = true;
                btnPBPlay.Text=";";
                lblPlayPause.Text = "pause";
                playVideo();
            }
            else
            {
                uint iOutValue = 0;
                btnPBPlay.Text = "4";
                lblPlayPause.Text = "play";
                if (!CHCNetSDK.NET_DVR_PlayBackControl_V40(m_lPlayHandle, CHCNetSDK.NET_DVR_PLAYPAUSE, IntPtr.Zero, 0, IntPtr.Zero, ref iOutValue))
                {
                    iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                    str = "NET_DVR_PLAYPAUSE failed, error code= " + iLastErr; //Playback controlling failed,print error code
                    MessageBox.Show(str);
                    return;
                }
            }
            
        }

        private void btnPBFast_Click(object sender, EventArgs e)
        {
            uint iOutValue = 0;

            if (!CHCNetSDK.NET_DVR_PlayBackControl_V40(m_lPlayHandle, CHCNetSDK.NET_DVR_PLAYFAST, IntPtr.Zero, 0, IntPtr.Zero, ref iOutValue))
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_PLAYFAST failed, error code= " + iLastErr; //Playback controlling failed,print error code
                MessageBox.Show(str);
                return;
            }
        }

        private void btnPBStop_Click(object sender, EventArgs e)
        {
            btnPBPlay.Text = "4";
            btnPBCapture.Enabled = false;
            pbPlaybackView.Image = null;
            stopVideo();
        }

        private void btnPBSlow_Click(object sender, EventArgs e)
        {
            uint iOutValue = 0;

            if (!CHCNetSDK.NET_DVR_PlayBackControl_V40(m_lPlayHandle, CHCNetSDK.NET_DVR_PLAYSLOW, IntPtr.Zero, 0, IntPtr.Zero, ref iOutValue))
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_PLAYSLOW failed, error code= " + iLastErr; //Playback controlling failed,print error code
                MessageBox.Show(str);
                return;
            }
        }

        private void dtpEnd_ValueChanged(object sender, EventArgs e)
        {
            btnPBFast.Enabled = true;
            btnPBPlay.Enabled = true;
            btnPBSlow.Enabled = true;
            btnPBStop.Enabled = true;
            if (videoPlaying)
                stopVideo();
        }

        private void playVideo()
        {
            m_lUserID = 0;
            CHCNetSDK.NET_DVR_VOD_PARA struVodPara = new CHCNetSDK.NET_DVR_VOD_PARA();
            struVodPara.dwSize = (uint)Marshal.SizeOf(struVodPara);
            struVodPara.struIDInfo.dwChannel = (uint)camera.channel; //Channel number  
            struVodPara.hWnd = pbPlaybackView.Handle;//handle of playback

            //Set the starting time to search video files
            struVodPara.struBeginTime.dwYear = (uint)dtpStart.Value.Year;
            struVodPara.struBeginTime.dwMonth = (uint)dtpStart.Value.Month;
            struVodPara.struBeginTime.dwDay = (uint)dtpStart.Value.Day;
            struVodPara.struBeginTime.dwHour = (uint)dtpStart.Value.Hour;
            struVodPara.struBeginTime.dwMinute = (uint)dtpStart.Value.Minute;
            struVodPara.struBeginTime.dwSecond = (uint)dtpStart.Value.Second;

            //Set the stopping time to search video files
            struVodPara.struEndTime.dwYear = (uint)dtpEnd.Value.Year;
            struVodPara.struEndTime.dwMonth = (uint)dtpEnd.Value.Month;
            struVodPara.struEndTime.dwDay = (uint)dtpEnd.Value.Day;
            struVodPara.struEndTime.dwHour = (uint)dtpEnd.Value.Hour;
            struVodPara.struEndTime.dwMinute = (uint)dtpEnd.Value.Minute;
            struVodPara.struEndTime.dwSecond = (uint)dtpEnd.Value.Second;

            //Playback by time
            m_lPlayHandle = CHCNetSDK.NET_DVR_PlayBackByTime_V40(m_lUserID, ref struVodPara);
            if (m_lPlayHandle < 0)
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_PlayBackByTime_V40 failed, error code= " + iLastErr;
                MessageBox.Show(str);
                return;
            }

            uint iOutValue = 0;
            if (!CHCNetSDK.NET_DVR_PlayBackControl_V40(m_lPlayHandle, CHCNetSDK.NET_DVR_PLAYSTART, IntPtr.Zero, 0, IntPtr.Zero, ref iOutValue))
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_PLAYSTART failed, error code= " + iLastErr; //Playback controlling failed,print error code.
                MessageBox.Show(str);
                return;
            }
        }

        private void stopVideo()
        {
            videoPlaying = false;
            if (!CHCNetSDK.NET_DVR_StopPlayBack(m_lPlayHandle))
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_StopPlayBack failed, error code= " + iLastErr;
                MessageBox.Show(str);
                return;
            }
        }

        private void btnPBCapture_Click(object sender, EventArgs e)
        {
            if (m_lPlayHandle < 0)
            {
                MessageBox.Show("Please start playback firstly!"); //Playback should be started before BMP Snapshot
                return;
            }

            string sBmpPicFileName = rootPath + camera.Location + "_" + DateTime.Now.ToString("yyyy-MM-dd_HHmmss") + ".bmp";
            string sJpgPicFileName = rootPath + camera.Location + "_" + DateTime.Now.ToString("yyyy-MM-dd_HHmmss") + ".jpg";

            //Capture a BMP picture
            if (!CHCNetSDK.NET_DVR_PlayBackCaptureFile(m_lPlayHandle, sBmpPicFileName))
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_PlayBackCaptureFile failed, error code= " + iLastErr;
                MessageBox.Show(str);
                return;
            }
            else
            {
                Thread.Sleep(1000);
                using (Image bm = Image.FromFile(sBmpPicFileName))
                {
                    convertToJpg(bm, sJpgPicFileName);
                }
                Thread.Sleep(1000);
                File.Delete(sBmpPicFileName);
            }
            return;
        }

        private void convertToJpg(Image bmp,string fileName)
        {
            var qualityEncoder = Encoder.Quality;
            var quality = (long)100;
            var ratio = new EncoderParameter(qualityEncoder, quality);
            var codecParams = new EncoderParameters(1);
            codecParams.Param[0] = ratio;
            ImageCodecInfo jpegCodecInfo = GetEncoder(ImageFormat.Jpeg);
            bmp.Save(fileName, jpegCodecInfo, codecParams);
        }

        private ImageCodecInfo GetEncoder(ImageFormat format)
        {
            ImageCodecInfo[] codecs = ImageCodecInfo.GetImageEncoders();
            foreach (ImageCodecInfo codec in codecs)
            {
                if (codec.FormatID == format.Guid)
                {
                    return codec;
                }
            }
            return null;
        }
    }
}
