﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Surveilance_WForms
{
    public class Camera
    {
        private int ID { get; set; }
        public String Location { get; set; }
        public bool Enabled { get; set; }
        public bool PTOption { get; set; }
        public bool ZoomOption { get; set; }
        public int handle { get; set; }
        public bool ALPROption { get; set; }
        public int channel { get; set; }
        public bool Started { get; set; }

        private String name;
        public String Name { set => name = "Camera" + this.ID; get => name; }

        public Camera (int cameraNumber)
        {
            this.ID = cameraNumber;
        }

    }
}
