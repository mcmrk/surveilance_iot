﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Surveilance_WForms
{
    class PTZValues
    {
        public int Pan { get; set; }
        public int Tilt { get; set; }
        public int Zoom { get; set; }
    }
}
