﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text.RegularExpressions;
using System.Windows.Forms;
using System.Xml;

namespace Surveilance_WForms
{
    public partial class Surveilance_App : Form
    {
        private Dictionary<Camera, PictureBox> cameraPool = new Dictionary<Camera, PictureBox>();
        private CheckBox selectedCB = null;
        private PictureBox selectedPB = null;
        private Timer screenshotTimer;
        public uint iLastErr = 0;
        public Int32 m_lUserID = -1;
        public bool m_bInitSDK = false;
        public bool m_bRecord = false;
        public bool m_bTalk = false;
        public Int32 m_lRealHandle = -1;
        public string str;
        public string capabilities="";
        public string xmlInput;
        public uint m_dwAbilityType = 0;
        public const String IP = "172.60.0.110";
        public const String port = "8000";
        public const String username = "admin";
        public const String password = "Pa$$w0rd";
        public bool autoCapture = false;
        //public const String rootPath = "H:\\Fax\\Algebra\\4. godina\\IoT\\surveilance_iot\\Surveilance_WForms\\Surveilance_WForms\\Surveilance_WForms\\Screenshots\\";
        public const String rootPath = "E:\\Fax\\Algebra\\4. godina\\IoT\\Surveilance_project\\Surveilance_WForms\\Surveilance_WForms\\Surveilance_WForms\\Screenshots\\";
        public List<CHCNetSDK.REALDATACALLBACK> rdList = new List<CHCNetSDK.REALDATACALLBACK>();
        public CHCNetSDK.REALDATACALLBACK RealData = null;
        public CHCNetSDK.NET_DVR_PTZPOS m_struPtzCfg;
        public bool PTZ3selected = false;
        public bool PTZ4selected = false;
        
        public Surveilance_App()
        {
            InitializeComponent();
            m_bInitSDK = CHCNetSDK.NET_DVR_Init();
            login();
            getCapabilities();
            InitTimer();
        }

        private void login()
        {
            CHCNetSDK.NET_DVR_DEVICEINFO_V30 DeviceInfo = new CHCNetSDK.NET_DVR_DEVICEINFO_V30();
            m_lUserID = CHCNetSDK.NET_DVR_Login_V30(IP, int.Parse(port), username, password,ref DeviceInfo);
        }

        private void getCapabilities()
        {
            IntPtr pInBuf;
            Int32 nSize;
            if (xmlInput == null)
            {
                pInBuf = IntPtr.Zero;
                nSize = 0;
            }
            else
            {
                nSize = xmlInput.Length;
                pInBuf = Marshal.AllocHGlobal(nSize);
                pInBuf = Marshal.StringToHGlobalAnsi(xmlInput);
            }

            int XML_ABILITY_OUT_LEN = 3 * 1024 * 1024;
            IntPtr pOutBuf = Marshal.AllocHGlobal(XML_ABILITY_OUT_LEN);

            CHCNetSDK.NET_DVR_GetDeviceAbility(m_lUserID, m_dwAbilityType, pInBuf, (uint)nSize, pOutBuf, (uint)XML_ABILITY_OUT_LEN);
            string strOutBuf = Marshal.PtrToStringAnsi(pOutBuf, XML_ABILITY_OUT_LEN);
            strOutBuf = strOutBuf.Replace(">\n<", ">\r\n<");
            Console.WriteLine(strOutBuf);
        }

        private bool cameraInPool(Camera camera)
        {
            bool match = false;
            foreach (KeyValuePair<Camera,PictureBox> pair in cameraPool)
                {
                    if (pair.Key.Name==camera.Name)
                    {
                        match = true;
                        break;
                    }
                }
            return match;
        }

        private Camera getCamera(int id)
        {
            Camera currentCamera = new Camera(id);
            if (id == 1)
            {
                currentCamera.Name = "Camera1";
                currentCamera.Location = "Algebra1";
                currentCamera.channel = 33;
                currentCamera.ALPROption = false;
                currentCamera.PTOption = false;
                currentCamera.ZoomOption = false;
                currentCamera.handle = m_lRealHandle;
            }
            if (id == 2)
            {
                currentCamera.Name = "Camera2";
                currentCamera.Location = "Algebra2";
                currentCamera.channel = 34;
                currentCamera.ALPROption = false;
                currentCamera.PTOption = false;
                currentCamera.ZoomOption = false;
                currentCamera.handle = m_lRealHandle;
            }
            if (id == 3)
            {
                currentCamera.Name = "Camera3";
                currentCamera.Location = "Algebra3";
                currentCamera.channel = 35;
                currentCamera.ALPROption = false;
                currentCamera.PTOption = true;
                currentCamera.ZoomOption = false;
                currentCamera.handle = m_lRealHandle;
            }
            if (id == 4)
            {
                currentCamera.Name = "Camera4";
                currentCamera.Location = "Velesajam";
                currentCamera.channel = 36;
                currentCamera.ALPROption = true;
                currentCamera.PTOption = false;
                currentCamera.ZoomOption = true;
                currentCamera.handle = m_lRealHandle;
            }
            return currentCamera;
        }

        private string GetErrorDescription(uint iErrCode)
        {
            string strDescription = "";
            switch (iErrCode)
            {
                case 1000:
                    strDescription = "Not Support";
                    break;
                case 1001:
                    strDescription = "insufficient memory";
                    break;
                case 1002:
                    strDescription = "Unable to find the corresponding local XML";
                    break;
                case 1003:
                    strDescription = "Error loading local XML";
                    break;
                case 1004:
                    strDescription = "Data format of device capability is incorrect";
                    break;
                case 1005:
                    strDescription = "The type of capability is error";
                    break;
                case 1006:
                    strDescription = "XML Capability Format is erro";
                    break;
                case 1007:
                    strDescription = "Input Capability XML Value is error";
                    break;
                case 1008:
                    strDescription = "XML version is mismatch";
                    break;
                default:
                    break;
            }
            return strDescription;
        }

        private string getCameraInfo(int channel)
        {
            m_dwAbilityType = CHCNetSDK.DEVICE_ABILITY_INFO;
            xmlInput = "<EventAbility version='2.0'><channelNO>"+channel+"</channelNO></EventAbility>";
            IntPtr pInBuf;
            Int32 nSize;
            if (xmlInput == null)
            {
                pInBuf = IntPtr.Zero;
                nSize = 0;
            }
            else
            {
                nSize = xmlInput.Length;
                pInBuf = Marshal.AllocHGlobal(nSize);
                pInBuf = Marshal.StringToHGlobalAnsi(xmlInput);
            }

            int XML_ABILITY_OUT_LEN = 3 * 1024 * 1024;
            IntPtr pOutBuf = Marshal.AllocHGlobal(XML_ABILITY_OUT_LEN);

            if (!CHCNetSDK.NET_DVR_GetDeviceAbility(m_lUserID, m_dwAbilityType, pInBuf, (uint)nSize, pOutBuf, (uint)XML_ABILITY_OUT_LEN))
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_GetDeviceAbility failed, error code= " + iLastErr + "\r\n" + GetErrorDescription(iLastErr);
                // Failed to get the capability set and output the error code
                capabilities = str;
            }
            else
            {
                string strOutBuf = Marshal.PtrToStringAnsi(pOutBuf, XML_ABILITY_OUT_LEN);
                strOutBuf = strOutBuf.Replace(">\n<", ">\r\n<");
                capabilities = strOutBuf;
            }
            Marshal.FreeHGlobal(pInBuf);
            Marshal.FreeHGlobal(pOutBuf);
            return capabilities;
        }

        private Dictionary<string,bool> parseXML(string xml)
        {
            Dictionary<string, bool> deviceParams =new Dictionary<string, bool>();
            Match m = Regex.Match(xml, "<ptzsupport>\\d", RegexOptions.IgnoreCase);
            if (m != null)
            {
                if (int.Parse(m.ToString().Substring(m.Length - 1)) == 0)
                {
                    deviceParams.Add("ptz", false);
                }
                else
                {
                    deviceParams.Add("ptz", true);
                }
            }
            return deviceParams;
        }

        private void Cam_btn_Click(object sender, EventArgs e)
        {
            String cameraNumber = (sender as Button).Text.Substring((sender as Button).Text.Length - 1);
            Camera camera = getCamera(Int32.Parse(cameraNumber));
            if (cameraInPool(camera))
            {
                (sender as Button).Image = Properties.Resources.red_dot;
                stopCamera(camera);
                var key = (from item in cameraPool
                           where item.Key.Name == camera.Name
                           select item.Key).FirstOrDefault();
                cameraPool.Remove(key);
                updateLocation(camera, int.Parse(selectedCB.Name.Substring(selectedCB.Name.Length - 1)));
                selectedCB.Checked = false;
            }
            else
            {
                if (cbCamera1.Checked == true || cbCamera2.Checked == true || cbCamera3.Checked == true || cbCamera4.Checked == true)
                {
                    lblMessage.Text = "";
                    (sender as Button).Image = Properties.Resources.green_dot;
                    cameraPool.Add(camera,selectedPB);
                    startCamera(camera, selectedPB);
                    //camera.PTZOption = parseXML(getCameraInfo(camera.channel))["ptz"];
                    updateLocation(camera,int.Parse(selectedCB.Name.Substring(selectedCB.Name.Length-1)));
                }
                else
                {
                    lblMessage.Text = "Please select a window to display view";
                }
            }
            togglePTZControls(selectedPB);
        }

        private void updateLocation(Camera camera,int windowID)
        {
            String location = "Location: ";
            if (cameraInPool(camera))
            {
                location += camera.Location;
            }
                switch (windowID)
                {
                    case 1:
                        lblCamera1.Text = location;
                        break;
                    case 2:
                        lblCamera2.Text = location;
                        break;
                    case 3:
                        lblCamera3.Text = location;
                        break;
                    case 4:
                        lblCamera4.Text = location;
                        break;
                }
        }

        private void stopCamera(Camera camera)
        {
            if (!CHCNetSDK.NET_DVR_StopRealPlay(m_lRealHandle))
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_StopRealPlay failed, error code= " + iLastErr;
                MessageBox.Show(str);
                return;
            }
            m_lRealHandle = -1;
        }

        private void startCamera(Camera camera, PictureBox playWindow)
        {
                CHCNetSDK.NET_DVR_PREVIEWINFO lpPreviewInfo = new CHCNetSDK.NET_DVR_PREVIEWINFO
                {
                    hPlayWnd = playWindow.Handle,
                    lChannel = camera.channel,
                    dwStreamType = 0,
                    dwLinkMode = 0,
                    bBlocked = true,
                    dwDisplayBufNum = 1,
                    byProtoType = 0,
                    byPreviewMode = 0
                };

                RealData = new CHCNetSDK.REALDATACALLBACK(RealDataCallBack);
                rdList.Add(RealData);

                IntPtr pUser = new IntPtr();

                //Start live view 
                m_lRealHandle = CHCNetSDK.NET_DVR_RealPlay_V40(m_lUserID, ref lpPreviewInfo, null/*RealData*/, pUser);
                if (m_lRealHandle < 0)
                {
                    iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                    str = "NET_DVR_RealPlay_V40 failed, error code= " + iLastErr;
                    MessageBox.Show(str);
                    return;
                }
                else
                {
                    //btnPreview.Text = "Stop Live View";
                }
            return;
        }

        public void RealDataCallBack(Int32 lRealHandle, UInt32 dwDataType, IntPtr pBuffer, UInt32 dwBufSize, IntPtr pUser)
        {
            if (dwBufSize > 0)
            {
                byte[] sData = new byte[dwBufSize];
                Marshal.Copy(pBuffer, sData, 0, (Int32)dwBufSize);
                string str = "ĘµĘ±Á÷ĘýľÝ.ps";
                FileStream fs = new FileStream(str, FileMode.Create);
                int iLen = (int)dwBufSize;
                fs.Write(sData, 0, iLen);
                fs.Close();
            }
        }

        private void cb_Click(object sender, EventArgs e)
        {
            cbCamera1.Checked = false;
            cbCamera2.Checked = false;
            cbCamera3.Checked = false;
            cbCamera4.Checked = false;
            (sender as CheckBox).Checked = true;
            if (cbCamera1.Checked == true)
            {
                selectedCB = cbCamera1;
                selectedPB = pbCamera1;
            }
            else if (cbCamera2.Checked == true)
            {
                selectedCB = cbCamera2;
                selectedPB = pbCamera2;
            }
            else if (cbCamera3.Checked == true)
            {
                selectedCB = cbCamera3;
                selectedPB = pbCamera3;
            }
            else
            {
                selectedCB = cbCamera4;
                selectedPB = pbCamera4;
            }
            togglePTZControls(selectedPB);
        }

          private void takeScreenshot()
        {
            foreach (KeyValuePair<Camera, PictureBox> pair in cameraPool)
            {
                screenshot(pair.Key);
            }
        }
        private void btnCapture_Click(object sender, EventArgs e)
        {
            if (selectedPB != null)
            {
                lblMessage.Text = "";
                Camera camera = cameraPool.FirstOrDefault(x => x.Value.Name == selectedPB.Name).Key;
                screenshot(camera);
            }
            else
            {
                lblMessage.Text = "Please select a window";
            }
        }

        public void screenshot(Camera camera)
        {
            string sJpegPicFileName;
            sJpegPicFileName = rootPath + camera.Location + "_" + DateTime.Now.ToString("yyyy-MM-ddHHmmss") + ".jpg";
            int lChannel = camera.channel;
            CHCNetSDK.NET_DVR_JPEGPARA lpJpegPara = new CHCNetSDK.NET_DVR_JPEGPARA
            {
                wPicQuality = 0,
                wPicSize = 0xff
            };

            if (!CHCNetSDK.NET_DVR_CaptureJPEGPicture(m_lUserID, lChannel, ref lpJpegPara, sJpegPicFileName))
            {
                iLastErr = CHCNetSDK.NET_DVR_GetLastError();
                str = "NET_DVR_CaptureJPEGPicture failed, error code= " + iLastErr;
                MessageBox.Show(str);
            }
        }

        public void InitTimer()
        {
            screenshotTimer = new Timer();
            screenshotTimer.Tick += new EventHandler(timer_Tick);
            screenshotTimer.Interval = 5000;
            screenshotTimer.Start();
        }

        private void timer_Tick(object sender, EventArgs e)
        {
            if(autoCapture)
            takeScreenshot();
        }

        private void togglePTZControls(PictureBox pictureBox)
        {
            btnPTZDown.Enabled = false;
            btnPTZUp.Enabled = false;
            btnPTZLeft.Enabled = false;
            btnPTZRight.Enabled = false;
            btnZoomIn.Enabled = false;
            btnZoomOut.Enabled = false;
            Camera cam = cameraPool.FirstOrDefault(x => x.Value.Name == pictureBox.Name).Key;
            if (cam!=null) {
                if (cam.PTOption)
                {
                    btnPTZDown.Enabled = true;
                    btnPTZUp.Enabled = true;
                    btnPTZLeft.Enabled = true;
                    btnPTZRight.Enabled = true;
                    btnZoomIn.Enabled = true;
                    btnZoomOut.Enabled = true;
                }
                if (cam.ZoomOption)
                {
                    btnZoomIn.Enabled = true;
                    btnZoomOut.Enabled = true;
                }
                if (cam.Name.Equals("Camera3"))
                {
                    if (!PTZ4selected)
                    {
                        cam.handle = 0;
                        PTZ3selected = true;
                    }
                    else
                    {
                        cam.handle = 1;
                    }
                }
                else if (cam.Name.Equals("Camera4"))
                {
                    if (!PTZ3selected)
                    {
                        cam.handle = 0;
                        PTZ4selected = true;
                    }
                    else
                    {
                        cam.handle = 1;
                    }
                }
                m_lRealHandle = cam.handle;
            }
        }

        private void btnPTZUp_MouseDown(object sender, MouseEventArgs e)
        {
            CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.TILT_UP, 0, 4);
        }

        private void btnPTZUp_MouseUp(object sender, MouseEventArgs e)
        {
            CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.TILT_UP, 1, 4);
        }

        private void btnPTZDown_MouseDown(object sender, MouseEventArgs e)
        {
            CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.TILT_DOWN, 0, 4);
        }

        private void btnPTZDown_MouseUp(object sender, MouseEventArgs e)
        {
            CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.TILT_DOWN, 1, 4);
        }

        private void btnPTZLeft_MouseDown(object sender, MouseEventArgs e)
        {
            CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.PAN_LEFT, 0, 4);
        }

        private void btnPTZLeft_MouseUp(object sender, MouseEventArgs e)
        {
            CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.PAN_LEFT, 1, 4);
        }

        private void btnPTZRight_MouseDown(object sender, MouseEventArgs e)
        {
            CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.PAN_RIGHT, 0, 4);
        }

        private void btnPTZRight_MouseUp(object sender, MouseEventArgs e)
        {
            CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.PAN_RIGHT, 1, 4);
        }

        private void btnZoomOut_MouseDown(object sender, MouseEventArgs e)
        {
            CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.ZOOM_OUT, 0, 4);
        }

        private void btnZoomOut_MouseUp(object sender, MouseEventArgs e)
        {
            CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.ZOOM_OUT, 1, 4);
        }

        private void btnZoomIn_MouseDown(object sender, MouseEventArgs e)
        {
            CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.ZOOM_IN, 0, 4);
        }

        private void btnZoomIn_MouseUp(object sender, MouseEventArgs e)
        {
            CHCNetSDK.NET_DVR_PTZControlWithSpeed(m_lRealHandle, CHCNetSDK.ZOOM_IN, 1, 4);
        }

        private void cbAutomaticCapture_CheckedChanged(object sender, EventArgs e)
        {
            if (cbAutomaticCapture.Checked)
            {
                autoCapture = true;
                btnCapture.Enabled = false;
            }
            else
            {
                autoCapture = false;
                btnCapture.Enabled = true;
            }
        }

        

        private void btnPB1_Click(object sender, EventArgs e)
        {
            var pbForm = new PBForm(getCamera(1),this);
            pbForm.Show();
            //this.Hide();
        }

        private void btnPB2_Click(object sender, EventArgs e)
        {
            var pbForm = new PBForm(getCamera(2),this);
            pbForm.Show();
            //this.Hide();
        }

        private void btnPB3_Click(object sender, EventArgs e)
        {
            var pbForm = new PBForm(getCamera(3),this);
            pbForm.Show();
            //this.Hide();
        }

        private void btnPB4_Click(object sender, EventArgs e)
        {
            var pbForm = new PBForm(getCamera(4),this);
            pbForm.Show();
            //this.Hide();
        }
    }
}
