﻿namespace Surveilance_WForms
{
    partial class Surveilance_App
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Surveilance_App));
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.Cam_btn1 = new System.Windows.Forms.Button();
            this.Cam_btn2 = new System.Windows.Forms.Button();
            this.Cam_btn3 = new System.Windows.Forms.Button();
            this.Cam_btn4 = new System.Windows.Forms.Button();
            this.lblMessage = new System.Windows.Forms.Label();
            this.btnCapture = new System.Windows.Forms.Button();
            this.cbAutomaticCapture = new System.Windows.Forms.CheckBox();
            this.btnPB1 = new System.Windows.Forms.Button();
            this.btnPB2 = new System.Windows.Forms.Button();
            this.btnPB3 = new System.Windows.Forms.Button();
            this.btnPB4 = new System.Windows.Forms.Button();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.btnPTZDown = new System.Windows.Forms.Button();
            this.btnPTZLeft = new System.Windows.Forms.Button();
            this.btnPTZRight = new System.Windows.Forms.Button();
            this.btnPTZUp = new System.Windows.Forms.Button();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnZoomOut = new System.Windows.Forms.Button();
            this.btnZoomIn = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.lblCamera4 = new System.Windows.Forms.Label();
            this.cbCamera4 = new System.Windows.Forms.CheckBox();
            this.pbCamera4 = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.cbCamera3 = new System.Windows.Forms.CheckBox();
            this.lblCamera3 = new System.Windows.Forms.Label();
            this.pbCamera3 = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cbCamera2 = new System.Windows.Forms.CheckBox();
            this.lblCamera2 = new System.Windows.Forms.Label();
            this.pbCamera2 = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cbCamera1 = new System.Windows.Forms.CheckBox();
            this.lblCamera1 = new System.Windows.Forms.Label();
            this.pbCamera1 = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCamera4)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCamera3)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCamera2)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCamera1)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.tableLayoutPanel2);
            this.flowLayoutPanel2.Controls.Add(this.tableLayoutPanel3);
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(1006, 12);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(266, 737);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.Cam_btn1, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.Cam_btn2, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.Cam_btn3, 1, 9);
            this.tableLayoutPanel2.Controls.Add(this.Cam_btn4, 1, 11);
            this.tableLayoutPanel2.Controls.Add(this.lblMessage, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.btnCapture, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.cbAutomaticCapture, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnPB1, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.btnPB2, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this.btnPB3, 2, 9);
            this.tableLayoutPanel2.Controls.Add(this.btnPB4, 2, 11);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 13;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel2.Size = new System.Drawing.Size(263, 373);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // Cam_btn1
            // 
            this.Cam_btn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Cam_btn1.Image = ((System.Drawing.Image)(resources.GetObject("Cam_btn1.Image")));
            this.Cam_btn1.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Cam_btn1.Location = new System.Drawing.Point(84, 143);
            this.Cam_btn1.Name = "Cam_btn1";
            this.Cam_btn1.Size = new System.Drawing.Size(94, 44);
            this.Cam_btn1.TabIndex = 0;
            this.Cam_btn1.Text = "Camera1";
            this.Cam_btn1.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Cam_btn1.UseVisualStyleBackColor = true;
            this.Cam_btn1.Click += new System.EventHandler(this.Cam_btn_Click);
            // 
            // Cam_btn2
            // 
            this.Cam_btn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Cam_btn2.Image = ((System.Drawing.Image)(resources.GetObject("Cam_btn2.Image")));
            this.Cam_btn2.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Cam_btn2.Location = new System.Drawing.Point(84, 203);
            this.Cam_btn2.Name = "Cam_btn2";
            this.Cam_btn2.Size = new System.Drawing.Size(94, 44);
            this.Cam_btn2.TabIndex = 1;
            this.Cam_btn2.Text = "Camera2";
            this.Cam_btn2.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Cam_btn2.UseVisualStyleBackColor = true;
            this.Cam_btn2.Click += new System.EventHandler(this.Cam_btn_Click);
            // 
            // Cam_btn3
            // 
            this.Cam_btn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Cam_btn3.Image = ((System.Drawing.Image)(resources.GetObject("Cam_btn3.Image")));
            this.Cam_btn3.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Cam_btn3.Location = new System.Drawing.Point(84, 263);
            this.Cam_btn3.Name = "Cam_btn3";
            this.Cam_btn3.Size = new System.Drawing.Size(94, 44);
            this.Cam_btn3.TabIndex = 2;
            this.Cam_btn3.Text = "Camera3";
            this.Cam_btn3.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Cam_btn3.UseVisualStyleBackColor = true;
            this.Cam_btn3.Click += new System.EventHandler(this.Cam_btn_Click);
            // 
            // Cam_btn4
            // 
            this.Cam_btn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.Cam_btn4.Image = ((System.Drawing.Image)(resources.GetObject("Cam_btn4.Image")));
            this.Cam_btn4.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Cam_btn4.Location = new System.Drawing.Point(84, 323);
            this.Cam_btn4.Name = "Cam_btn4";
            this.Cam_btn4.Size = new System.Drawing.Size(94, 44);
            this.Cam_btn4.TabIndex = 3;
            this.Cam_btn4.Text = "Camera4";
            this.Cam_btn4.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.Cam_btn4.UseVisualStyleBackColor = true;
            this.Cam_btn4.Click += new System.EventHandler(this.Cam_btn_Click);
            // 
            // lblMessage
            // 
            this.lblMessage.AutoSize = true;
            this.lblMessage.Location = new System.Drawing.Point(84, 100);
            this.lblMessage.Name = "lblMessage";
            this.lblMessage.Padding = new System.Windows.Forms.Padding(0, 50, 0, 0);
            this.lblMessage.Size = new System.Drawing.Size(0, 40);
            this.lblMessage.TabIndex = 4;
            // 
            // btnCapture
            // 
            this.btnCapture.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnCapture.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnCapture.Location = new System.Drawing.Point(84, 53);
            this.btnCapture.Name = "btnCapture";
            this.btnCapture.Size = new System.Drawing.Size(94, 44);
            this.btnCapture.TabIndex = 5;
            this.btnCapture.Text = "Capture";
            this.btnCapture.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnCapture.UseVisualStyleBackColor = true;
            this.btnCapture.Click += new System.EventHandler(this.btnCapture_Click);
            // 
            // cbAutomaticCapture
            // 
            this.cbAutomaticCapture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cbAutomaticCapture.AutoSize = true;
            this.cbAutomaticCapture.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.cbAutomaticCapture.Location = new System.Drawing.Point(87, 16);
            this.cbAutomaticCapture.Name = "cbAutomaticCapture";
            this.cbAutomaticCapture.Size = new System.Drawing.Size(87, 17);
            this.cbAutomaticCapture.TabIndex = 6;
            this.cbAutomaticCapture.Text = "Auto capture";
            this.cbAutomaticCapture.UseVisualStyleBackColor = true;
            this.cbAutomaticCapture.CheckedChanged += new System.EventHandler(this.cbAutomaticCapture_CheckedChanged);
            // 
            // btnPB1
            // 
            this.btnPB1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPB1.Location = new System.Drawing.Point(184, 143);
            this.btnPB1.Name = "btnPB1";
            this.btnPB1.Size = new System.Drawing.Size(31, 44);
            this.btnPB1.TabIndex = 7;
            this.btnPB1.Text = "PB";
            this.btnPB1.UseVisualStyleBackColor = true;
            this.btnPB1.Click += new System.EventHandler(this.btnPB1_Click);
            // 
            // btnPB2
            // 
            this.btnPB2.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPB2.Location = new System.Drawing.Point(184, 203);
            this.btnPB2.Name = "btnPB2";
            this.btnPB2.Size = new System.Drawing.Size(31, 44);
            this.btnPB2.TabIndex = 8;
            this.btnPB2.Text = "PB";
            this.btnPB2.UseVisualStyleBackColor = true;
            this.btnPB2.Click += new System.EventHandler(this.btnPB2_Click);
            // 
            // btnPB3
            // 
            this.btnPB3.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPB3.Location = new System.Drawing.Point(184, 263);
            this.btnPB3.Name = "btnPB3";
            this.btnPB3.Size = new System.Drawing.Size(31, 44);
            this.btnPB3.TabIndex = 9;
            this.btnPB3.Text = "PB";
            this.btnPB3.UseVisualStyleBackColor = true;
            this.btnPB3.Click += new System.EventHandler(this.btnPB3_Click);
            // 
            // btnPB4
            // 
            this.btnPB4.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPB4.Location = new System.Drawing.Point(184, 323);
            this.btnPB4.Name = "btnPB4";
            this.btnPB4.Size = new System.Drawing.Size(31, 44);
            this.btnPB4.TabIndex = 10;
            this.btnPB4.Text = "PB";
            this.btnPB4.UseVisualStyleBackColor = true;
            this.btnPB4.Click += new System.EventHandler(this.btnPB4_Click);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Controls.Add(this.btnPTZDown, 2, 5);
            this.tableLayoutPanel3.Controls.Add(this.btnPTZLeft, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.btnPTZRight, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.btnPTZUp, 2, 1);
            this.tableLayoutPanel3.Controls.Add(this.flowLayoutPanel6, 2, 3);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 382);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 9;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(263, 318);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // btnPTZDown
            // 
            this.btnPTZDown.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnPTZDown.Enabled = false;
            this.btnPTZDown.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPTZDown.Location = new System.Drawing.Point(104, 173);
            this.btnPTZDown.Name = "btnPTZDown";
            this.btnPTZDown.Size = new System.Drawing.Size(54, 44);
            this.btnPTZDown.TabIndex = 5;
            this.btnPTZDown.Text = "˅";
            this.btnPTZDown.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPTZDown.UseVisualStyleBackColor = true;
            this.btnPTZDown.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnPTZDown_MouseDown);
            this.btnPTZDown.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnPTZDown_MouseUp);
            // 
            // btnPTZLeft
            // 
            this.btnPTZLeft.Enabled = false;
            this.btnPTZLeft.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPTZLeft.Location = new System.Drawing.Point(24, 113);
            this.btnPTZLeft.Name = "btnPTZLeft";
            this.btnPTZLeft.Size = new System.Drawing.Size(54, 44);
            this.btnPTZLeft.TabIndex = 6;
            this.btnPTZLeft.Text = "˂";
            this.btnPTZLeft.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPTZLeft.UseVisualStyleBackColor = true;
            this.btnPTZLeft.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnPTZLeft_MouseDown);
            this.btnPTZLeft.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnPTZLeft_MouseUp);
            // 
            // btnPTZRight
            // 
            this.btnPTZRight.Enabled = false;
            this.btnPTZRight.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPTZRight.Location = new System.Drawing.Point(184, 113);
            this.btnPTZRight.Name = "btnPTZRight";
            this.btnPTZRight.Size = new System.Drawing.Size(54, 44);
            this.btnPTZRight.TabIndex = 7;
            this.btnPTZRight.Text = "˃";
            this.btnPTZRight.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPTZRight.UseVisualStyleBackColor = true;
            this.btnPTZRight.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnPTZRight_MouseDown);
            this.btnPTZRight.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnPTZRight_MouseUp);
            // 
            // btnPTZUp
            // 
            this.btnPTZUp.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnPTZUp.Enabled = false;
            this.btnPTZUp.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPTZUp.Location = new System.Drawing.Point(104, 53);
            this.btnPTZUp.Name = "btnPTZUp";
            this.btnPTZUp.Size = new System.Drawing.Size(54, 44);
            this.btnPTZUp.TabIndex = 4;
            this.btnPTZUp.Text = "˄";
            this.btnPTZUp.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnPTZUp.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPTZUp.UseVisualStyleBackColor = true;
            this.btnPTZUp.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnPTZUp_MouseDown);
            this.btnPTZUp.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnPTZUp_MouseUp);
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.flowLayoutPanel6.Controls.Add(this.btnZoomOut);
            this.flowLayoutPanel6.Controls.Add(this.btnZoomIn);
            this.flowLayoutPanel6.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(84, 113);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(94, 44);
            this.flowLayoutPanel6.TabIndex = 8;
            // 
            // btnZoomOut
            // 
            this.btnZoomOut.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnZoomOut.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnZoomOut.Location = new System.Drawing.Point(3, 3);
            this.btnZoomOut.Name = "btnZoomOut";
            this.btnZoomOut.Size = new System.Drawing.Size(41, 41);
            this.btnZoomOut.TabIndex = 0;
            this.btnZoomOut.Text = "-";
            this.btnZoomOut.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnZoomOut.UseVisualStyleBackColor = true;
            this.btnZoomOut.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnZoomOut_MouseDown);
            this.btnZoomOut.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnZoomOut_MouseUp);
            // 
            // btnZoomIn
            // 
            this.btnZoomIn.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnZoomIn.Font = new System.Drawing.Font("Microsoft Sans Serif", 21.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnZoomIn.Location = new System.Drawing.Point(50, 3);
            this.btnZoomIn.Name = "btnZoomIn";
            this.btnZoomIn.Size = new System.Drawing.Size(41, 41);
            this.btnZoomIn.TabIndex = 2;
            this.btnZoomIn.Text = "+";
            this.btnZoomIn.UseVisualStyleBackColor = true;
            this.btnZoomIn.MouseDown += new System.Windows.Forms.MouseEventHandler(this.btnZoomIn_MouseDown);
            this.btnZoomIn.MouseUp += new System.Windows.Forms.MouseEventHandler(this.btnZoomIn_MouseUp);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel5, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel4, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel3, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(12, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(988, 737);
            this.tableLayoutPanel1.TabIndex = 2;
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel5.Controls.Add(this.panel4);
            this.flowLayoutPanel5.Controls.Add(this.pbCamera4);
            this.flowLayoutPanel5.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(497, 371);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(488, 362);
            this.flowLayoutPanel5.TabIndex = 7;
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.lblCamera4);
            this.panel4.Controls.Add(this.cbCamera4);
            this.panel4.Location = new System.Drawing.Point(3, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(485, 17);
            this.panel4.TabIndex = 3;
            // 
            // lblCamera4
            // 
            this.lblCamera4.AutoSize = true;
            this.lblCamera4.Location = new System.Drawing.Point(333, 4);
            this.lblCamera4.Name = "lblCamera4";
            this.lblCamera4.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCamera4.Size = new System.Drawing.Size(51, 13);
            this.lblCamera4.TabIndex = 1;
            this.lblCamera4.Text = "Location:";
            this.lblCamera4.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cbCamera4
            // 
            this.cbCamera4.AutoSize = true;
            this.cbCamera4.Location = new System.Drawing.Point(3, 0);
            this.cbCamera4.Name = "cbCamera4";
            this.cbCamera4.Size = new System.Drawing.Size(58, 17);
            this.cbCamera4.TabIndex = 5;
            this.cbCamera4.Text = "View 4";
            this.cbCamera4.UseVisualStyleBackColor = true;
            this.cbCamera4.Click += new System.EventHandler(this.cb_Click);
            // 
            // pbCamera4
            // 
            this.pbCamera4.Location = new System.Drawing.Point(3, 26);
            this.pbCamera4.Name = "pbCamera4";
            this.pbCamera4.Size = new System.Drawing.Size(485, 329);
            this.pbCamera4.TabIndex = 1;
            this.pbCamera4.TabStop = false;
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel4.Controls.Add(this.panel3);
            this.flowLayoutPanel4.Controls.Add(this.pbCamera3);
            this.flowLayoutPanel4.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(3, 371);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(488, 362);
            this.flowLayoutPanel4.TabIndex = 6;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cbCamera3);
            this.panel3.Controls.Add(this.lblCamera3);
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(485, 17);
            this.panel3.TabIndex = 3;
            // 
            // cbCamera3
            // 
            this.cbCamera3.AutoSize = true;
            this.cbCamera3.Location = new System.Drawing.Point(3, 0);
            this.cbCamera3.Name = "cbCamera3";
            this.cbCamera3.Size = new System.Drawing.Size(58, 17);
            this.cbCamera3.TabIndex = 4;
            this.cbCamera3.Text = "View 3";
            this.cbCamera3.UseVisualStyleBackColor = true;
            this.cbCamera3.Click += new System.EventHandler(this.cb_Click);
            // 
            // lblCamera3
            // 
            this.lblCamera3.AutoSize = true;
            this.lblCamera3.Location = new System.Drawing.Point(333, 4);
            this.lblCamera3.Name = "lblCamera3";
            this.lblCamera3.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCamera3.Size = new System.Drawing.Size(51, 13);
            this.lblCamera3.TabIndex = 1;
            this.lblCamera3.Text = "Location:";
            this.lblCamera3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pbCamera3
            // 
            this.pbCamera3.Location = new System.Drawing.Point(3, 26);
            this.pbCamera3.Name = "pbCamera3";
            this.pbCamera3.Size = new System.Drawing.Size(485, 329);
            this.pbCamera3.TabIndex = 1;
            this.pbCamera3.TabStop = false;
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel3.Controls.Add(this.panel2);
            this.flowLayoutPanel3.Controls.Add(this.pbCamera2);
            this.flowLayoutPanel3.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(497, 3);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(488, 362);
            this.flowLayoutPanel3.TabIndex = 5;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cbCamera2);
            this.panel2.Controls.Add(this.lblCamera2);
            this.panel2.Location = new System.Drawing.Point(3, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(485, 17);
            this.panel2.TabIndex = 3;
            // 
            // cbCamera2
            // 
            this.cbCamera2.AutoSize = true;
            this.cbCamera2.Location = new System.Drawing.Point(3, 0);
            this.cbCamera2.Name = "cbCamera2";
            this.cbCamera2.Size = new System.Drawing.Size(58, 17);
            this.cbCamera2.TabIndex = 3;
            this.cbCamera2.Text = "View 2";
            this.cbCamera2.UseVisualStyleBackColor = true;
            this.cbCamera2.Click += new System.EventHandler(this.cb_Click);
            // 
            // lblCamera2
            // 
            this.lblCamera2.AutoSize = true;
            this.lblCamera2.Location = new System.Drawing.Point(333, 4);
            this.lblCamera2.Name = "lblCamera2";
            this.lblCamera2.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCamera2.Size = new System.Drawing.Size(51, 13);
            this.lblCamera2.TabIndex = 1;
            this.lblCamera2.Text = "Location:";
            this.lblCamera2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pbCamera2
            // 
            this.pbCamera2.Location = new System.Drawing.Point(3, 26);
            this.pbCamera2.Name = "pbCamera2";
            this.pbCamera2.Size = new System.Drawing.Size(485, 329);
            this.pbCamera2.TabIndex = 1;
            this.pbCamera2.TabStop = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.flowLayoutPanel1.Controls.Add(this.panel1);
            this.flowLayoutPanel1.Controls.Add(this.pbCamera1);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(488, 362);
            this.flowLayoutPanel1.TabIndex = 4;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cbCamera1);
            this.panel1.Controls.Add(this.lblCamera1);
            this.panel1.Location = new System.Drawing.Point(3, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(485, 17);
            this.panel1.TabIndex = 2;
            // 
            // cbCamera1
            // 
            this.cbCamera1.AutoSize = true;
            this.cbCamera1.Location = new System.Drawing.Point(4, 0);
            this.cbCamera1.Name = "cbCamera1";
            this.cbCamera1.Size = new System.Drawing.Size(58, 17);
            this.cbCamera1.TabIndex = 2;
            this.cbCamera1.Text = "View 1";
            this.cbCamera1.UseVisualStyleBackColor = true;
            this.cbCamera1.Click += new System.EventHandler(this.cb_Click);
            // 
            // lblCamera1
            // 
            this.lblCamera1.AutoSize = true;
            this.lblCamera1.Location = new System.Drawing.Point(333, 4);
            this.lblCamera1.Name = "lblCamera1";
            this.lblCamera1.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.lblCamera1.Size = new System.Drawing.Size(51, 13);
            this.lblCamera1.TabIndex = 1;
            this.lblCamera1.Text = "Location:";
            this.lblCamera1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // pbCamera1
            // 
            this.pbCamera1.Location = new System.Drawing.Point(3, 26);
            this.pbCamera1.Name = "pbCamera1";
            this.pbCamera1.Size = new System.Drawing.Size(485, 329);
            this.pbCamera1.TabIndex = 1;
            this.pbCamera1.TabStop = false;
            // 
            // Surveilance_App
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(1284, 749);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.flowLayoutPanel2);
            this.Name = "Surveilance_App";
            this.Text = "Surveilance_App";
            this.flowLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.flowLayoutPanel5.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCamera4)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCamera3)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCamera2)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbCamera1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button Cam_btn1;
        private System.Windows.Forms.Button Cam_btn2;
        private System.Windows.Forms.Button Cam_btn3;
        private System.Windows.Forms.Button Cam_btn4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        private System.Windows.Forms.PictureBox pbCamera4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        private System.Windows.Forms.PictureBox pbCamera3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private System.Windows.Forms.PictureBox pbCamera2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pbCamera1;
        private System.Windows.Forms.Label lblCamera1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label lblCamera4;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblCamera3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblCamera2;
        private System.Windows.Forms.Label lblMessage;
        private System.Windows.Forms.CheckBox cbCamera1;
        private System.Windows.Forms.CheckBox cbCamera4;
        private System.Windows.Forms.CheckBox cbCamera3;
        private System.Windows.Forms.CheckBox cbCamera2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.Button btnPTZDown;
        private System.Windows.Forms.Button btnPTZLeft;
        private System.Windows.Forms.Button btnPTZRight;
        private System.Windows.Forms.Button btnPTZUp;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        private System.Windows.Forms.Button btnZoomOut;
        private System.Windows.Forms.Button btnZoomIn;
        private System.Windows.Forms.Button btnCapture;
        private System.Windows.Forms.CheckBox cbAutomaticCapture;
        private System.Windows.Forms.Button btnPB1;
        private System.Windows.Forms.Button btnPB2;
        private System.Windows.Forms.Button btnPB3;
        private System.Windows.Forms.Button btnPB4;
    }
}

