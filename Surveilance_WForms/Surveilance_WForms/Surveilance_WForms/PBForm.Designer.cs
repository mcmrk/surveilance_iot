﻿namespace Surveilance_WForms
{
    partial class PBForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.dtpStart = new System.Windows.Forms.DateTimePicker();
            this.dtpEnd = new System.Windows.Forms.DateTimePicker();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.lblPlayPause = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.btnPBFast = new System.Windows.Forms.Button();
            this.btnPBPlay = new System.Windows.Forms.Button();
            this.btnPBSlow = new System.Windows.Forms.Button();
            this.btnPBStop = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.btnPBCapture = new System.Windows.Forms.Button();
            this.pbPlaybackView = new System.Windows.Forms.PictureBox();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlaybackView)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Controls.Add(this.dtpStart, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.dtpEnd, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.btnPBCapture, 1, 6);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(503, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 8;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(242, 330);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // dtpStart
            // 
            this.dtpStart.CustomFormat = "dd-MM-yyyy HH:mm:ss";
            this.dtpStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStart.Location = new System.Drawing.Point(43, 23);
            this.dtpStart.Name = "dtpStart";
            this.dtpStart.Size = new System.Drawing.Size(156, 20);
            this.dtpStart.TabIndex = 0;
            this.dtpStart.ValueChanged += new System.EventHandler(this.dtpStart_ValueChanged);
            // 
            // dtpEnd
            // 
            this.dtpEnd.CustomFormat = "dd-MM-yyyy HH:mm:ss";
            this.dtpEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpEnd.Location = new System.Drawing.Point(43, 53);
            this.dtpEnd.Name = "dtpEnd";
            this.dtpEnd.Size = new System.Drawing.Size(156, 20);
            this.dtpEnd.TabIndex = 1;
            this.dtpEnd.Value = new System.DateTime(2020, 2, 23, 12, 32, 15, 0);
            this.dtpEnd.ValueChanged += new System.EventHandler(this.dtpEnd_ValueChanged);
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(5, 28);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Start:";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(8, 58);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(29, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "End:";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel2.Controls.Add(this.label6, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.lblPlayPause, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label4, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.btnPBFast, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnPBPlay, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnPBSlow, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.btnPBStop, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(43, 103);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 54F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(156, 54);
            this.tableLayoutPanel2.TabIndex = 4;
            // 
            // label6
            // 
            this.label6.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label6.Location = new System.Drawing.Point(126, 38);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(21, 12);
            this.label6.TabIndex = 7;
            this.label6.Text = "fast";
            // 
            // lblPlayPause
            // 
            this.lblPlayPause.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.lblPlayPause.AutoSize = true;
            this.lblPlayPause.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.lblPlayPause.Location = new System.Drawing.Point(86, 38);
            this.lblPlayPause.Name = "lblPlayPause";
            this.lblPlayPause.Size = new System.Drawing.Size(22, 12);
            this.lblPlayPause.TabIndex = 6;
            this.lblPlayPause.Text = "play";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label4.Location = new System.Drawing.Point(47, 38);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(23, 12);
            this.label4.TabIndex = 5;
            this.label4.Text = "stop";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPBFast
            // 
            this.btnPBFast.Enabled = false;
            this.btnPBFast.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnPBFast.Location = new System.Drawing.Point(120, 3);
            this.btnPBFast.Name = "btnPBFast";
            this.btnPBFast.Size = new System.Drawing.Size(33, 28);
            this.btnPBFast.TabIndex = 3;
            this.btnPBFast.Text = "8";
            this.btnPBFast.UseVisualStyleBackColor = true;
            this.btnPBFast.Click += new System.EventHandler(this.btnPBFast_Click);
            // 
            // btnPBPlay
            // 
            this.btnPBPlay.Enabled = false;
            this.btnPBPlay.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnPBPlay.Location = new System.Drawing.Point(81, 3);
            this.btnPBPlay.Name = "btnPBPlay";
            this.btnPBPlay.Size = new System.Drawing.Size(33, 28);
            this.btnPBPlay.TabIndex = 2;
            this.btnPBPlay.Text = "4";
            this.btnPBPlay.UseVisualStyleBackColor = true;
            this.btnPBPlay.Click += new System.EventHandler(this.btnPBPlay_Click);
            // 
            // btnPBSlow
            // 
            this.btnPBSlow.Enabled = false;
            this.btnPBSlow.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnPBSlow.Location = new System.Drawing.Point(3, 3);
            this.btnPBSlow.Name = "btnPBSlow";
            this.btnPBSlow.Size = new System.Drawing.Size(33, 28);
            this.btnPBSlow.TabIndex = 0;
            this.btnPBSlow.Text = "7";
            this.btnPBSlow.UseVisualStyleBackColor = true;
            this.btnPBSlow.Click += new System.EventHandler(this.btnPBSlow_Click);
            // 
            // btnPBStop
            // 
            this.btnPBStop.Enabled = false;
            this.btnPBStop.Font = new System.Drawing.Font("Webdings", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.btnPBStop.Location = new System.Drawing.Point(42, 3);
            this.btnPBStop.Name = "btnPBStop";
            this.btnPBStop.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.btnPBStop.Size = new System.Drawing.Size(33, 28);
            this.btnPBStop.TabIndex = 1;
            this.btnPBStop.Text = "<";
            this.btnPBStop.UseVisualStyleBackColor = true;
            this.btnPBStop.Click += new System.EventHandler(this.btnPBStop_Click);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.label3.Location = new System.Drawing.Point(7, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(25, 12);
            this.label3.TabIndex = 4;
            this.label3.Text = "slow";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnPBCapture
            // 
            this.btnPBCapture.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.btnPBCapture.Enabled = false;
            this.btnPBCapture.Location = new System.Drawing.Point(73, 183);
            this.btnPBCapture.Name = "btnPBCapture";
            this.btnPBCapture.Size = new System.Drawing.Size(95, 44);
            this.btnPBCapture.TabIndex = 5;
            this.btnPBCapture.Text = "Capture";
            this.btnPBCapture.UseVisualStyleBackColor = true;
            this.btnPBCapture.Click += new System.EventHandler(this.btnPBCapture_Click);
            // 
            // pbPlaybackView
            // 
            this.pbPlaybackView.BackColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.pbPlaybackView.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pbPlaybackView.Location = new System.Drawing.Point(12, 12);
            this.pbPlaybackView.Name = "pbPlaybackView";
            this.pbPlaybackView.Size = new System.Drawing.Size(485, 330);
            this.pbPlaybackView.TabIndex = 1;
            this.pbPlaybackView.TabStop = false;
            // 
            // PBForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlDark;
            this.ClientSize = new System.Drawing.Size(754, 353);
            this.Controls.Add(this.pbPlaybackView);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "PBForm";
            this.Text = "PlayBack View";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.PBForm_FormClosing);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbPlaybackView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.DateTimePicker dtpStart;
        private System.Windows.Forms.DateTimePicker dtpEnd;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pbPlaybackView;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.Button btnPBFast;
        private System.Windows.Forms.Button btnPBPlay;
        private System.Windows.Forms.Button btnPBStop;
        private System.Windows.Forms.Button btnPBSlow;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblPlayPause;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnPBCapture;
    }
}